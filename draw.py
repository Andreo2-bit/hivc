import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import math
data = pd.read_csv('sol.dat')
disp = pd.read_csv('disp')

patient = 1
virus = pd.read_excel('data_app_new.xlsx')
tM = [virus[str('TP') + str(patient)].values]
CDM = [np.log10(x) for x in virus[str('CDP') + str(patient)].values]
#VM = [virus[str('VP') + str(patient)].values]
virus['V'] = np.power(10, virus[str('VP') + str(patient)])/2000
VM = [np.log10(x) for x in [virus[str('V')]]]
t = data['t'].values


fig, ax = plt.subplots()
ax.set(title = 'CD4')
CDP = data['T'].values
CDP = np.log10(CDP)
ax.plot(t, CDP, color = (1.0, 0.0, 0.0))
ax.scatter(tM, CDM, s = 10)
ax.grid()
fig.tight_layout()
fig.set_size_inches(15.5, 3.5, forward=True)


fig, ax = plt.subplots()
ax.set(title = 'CDV')
CDV = data['T*'].values
ax.plot(t, CDV, color = (1.0, 0.0, 0.0))
ax.grid()
fig.tight_layout()
fig.set_size_inches(15.5, 3.5, forward=True)


fix, ax = plt.subplots()
ax.set(title = 'CTL')
CTL = data['CTL'].values
#CTL = np.log10(CTL)
ax.plot(t, CTL, color = (1.0, 0.0, 0.0))
ax.grid()
fig.tight_layout()
fig.set_size_inches(15.5, 3.5, forward=True)


fig, ax = plt.subplots()
ax.set(title = 'V')
V = data['V'].values
for x in np.nditer(V, op_flags = ['readwrite']):
    x[...] = x
V = np.log10(V)

ax.plot(t, V, color = (1.0, 0.0, 0.0))
ax.scatter(tM, VM, s = 10)
ax.grid()
fig.tight_layout()
fig.set_size_inches(15.5, 3.5, forward=True)

K1 = disp['k1'].values
K8 = disp['k8'].values
P8 = disp['p8'].values
PV = disp['pV'].values
R = disp['r'].values

'''
sigmaK1 = np.zeros(1000)
sigmaK8 = np.zeros(1000)
sigmaP8 = np.zeros(1000)
sigmaPV = np.zeros(1000)
sigmaR = np.zeros(1000)
for i in range(1, 1000):
    meanK1 = 0
    meanK8 = 0
    meanP8 = 0
    meanPV = 0
    meanR = 0
    for j in range(0,i+1):
        meanK1 = meanK1 + K1[j]
        meanK8 = meanK8 + K8[j]
        meanP8 = meanP8 + P8[j]
        meanPV = meanPV + PV[j]
        meanR = meanR + R[j]
    meanK1 = meanK1/1000
    meanK8 = meanK8/1000
    meanP8 = meanP8/1000
    meanPV = meanPV/1000
    meanR = meanR/1000
    for j in range(0,i+1):
        sigmaK1[i] += (K1[j] - meanK1)**2
        sigmaK8[i] += (K8[j] - meanK8)**2
        sigmaP8[i] += (P8[j] - meanP8)**2
        sigmaPV[i] += (PV[j] - meanPV)**2
        sigmaR[i] += (R[j] - meanR)**2
    
    sigmaK1[i] = (sigmaK1[i]/i+2)**(1/2)
    sigmaK8[i] = (sigmaK8[i]/i+2)**(1/2)
    sigmaP8[i] = (sigmaP8[i]/i+2)**(1/2)
    sigmaPV[i] = (sigmaPV[i]/i+2)**(1/2)
    sigmaR[i] = (sigmaR[i]/i+2)**(1/2)

sigmaT = np.arange(1, 1000)
fig, ax = plt.subplots()
sigmaK1 = sigmaK1[1:]'''

sigmaT = np.arange(1, 5000)
fig, ax = plt.subplots()
ax.set(title = 'k1')
ax.scatter(sigmaT, K1, s = 5)
ax.grid()
fig.tight_layout()
fig.set_size_inches(15.5, 3.5, forward=True)

fig, ax = plt.subplots()
ax.set(title = 'k8')
ax.scatter(sigmaT, K8, s = 5)
ax.grid()
fig.tight_layout()
fig.set_size_inches(15.5, 3.5, forward=True)

fig, ax = plt.subplots()
ax.set(title = 'p8')
ax.scatter(sigmaT, P8, s = 5)
ax.grid()
fig.tight_layout()
fig.set_size_inches(15.5, 3.5, forward=True)

fig, ax = plt.subplots()
ax.set(title = 'pV')
ax.scatter(sigmaT, PV, s = 5)
ax.grid()
fig.tight_layout()
fig.set_size_inches(15.5, 3.5, forward=True)

fig, ax = plt.subplots()
ax.set(title = 'r')
ax.scatter(sigmaT, R, s = 5)
ax.grid()
fig.tight_layout()
fig.set_size_inches(15.5, 3.5, forward=True)
plt.show()
