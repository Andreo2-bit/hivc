gcc -g -I ~/gsl-2.6/gsl main.c -static -lgsl -lgslcblas -lm -o exec
gcc -g -I ~/gsl-2.6/gsl min.c -static -lgsl -lgslcblas -lm -o execMin
gcc -O3 -I ~/gsl-2.6/gsl trs.c -static -lgsl -lgslcblas -lm -o execTrs
