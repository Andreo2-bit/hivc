#include"gsl_odeiv2.h"
#include"gsl_multifit_nlinear.h"
#include"gsl_multimin.h"
#include"gsl_matrix.h"
#include"gsl_math.h"
#include"gsl_vector.h"
#include"gsl_blas.h"
#include"gsl_rng.h"
#include"gsl_randist.h"
#include"gsl_errno.h"
#include<stdio.h>
#include<stdlib.h>
int rhsF(double t, const double y[], double f[], void *params){
    double *buf = (double*)params;
    double k = 1 - (y[0]+y[1])/buf[6];
    f[0] = buf[0] + buf[2]*y[0]*k - buf[3]*y[0]*y[3] - buf[4]*y[0];
    f[1] = buf[3]*y[0]*y[3] + buf[7]*y[1]*k - buf[8]*y[1]*y[2] - buf[5]*y[1];
    f[2] = buf[1] + buf[9]*y[1]*y[2] - buf[10]*y[2];
    f[3] = buf[12]*y[1] - buf[13]*y[0]*y[3] - buf[11]*y[3];
    return  GSL_SUCCESS;
}

int jacF(double t, const double y[], double *dfdy, double dfdt[], void *params){
    (void) (t);
    double *buf = (double*)params;
    gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, 4, 4);
    gsl_matrix *m = &dfdy_mat.matrix;
    gsl_matrix_set(m, 0, 0, buf[2]*(1 - (y[0]+y[1])/buf[6]) - buf[2]*y[0]/buf[6]
        - buf[3]*y[3] - buf[4]);
    gsl_matrix_set(m, 0, 1, -1.0*buf[2]*y[0]/buf[6]);
    gsl_matrix_set(m, 0, 2, 0.0);
    gsl_matrix_set(m, 0, 3, -1.0*buf[3]*y[0]);
    gsl_matrix_set(m, 1, 0, buf[3]*y[3] - buf[7]*y[1]/buf[6]);
    gsl_matrix_set(m, 1, 1, buf[7]*(1 - (y[0]+y[1])/buf[6]) - buf[7]*y[1]/buf[6]
        - buf[8]*y[2] - buf[5]);
    gsl_matrix_set(m, 1, 2, -1.0*buf[8]*y[1]);
    gsl_matrix_set(m, 1, 3, buf[3]*y[0]);
    gsl_matrix_set(m, 2, 0, 0.0);
    gsl_matrix_set(m, 2, 1, buf[9]*y[2]);
    gsl_matrix_set(m, 2, 2, buf[9]*y[1] - buf[10]);
    gsl_matrix_set(m, 2, 3, 0.0);
    gsl_matrix_set(m, 3, 0, buf[13]*y[3]);
    gsl_matrix_set(m, 3, 1, buf[12]);
    gsl_matrix_set(m, 3, 2, 0.0);
    gsl_matrix_set(m, 3, 3, -1.0*buf[13]*y[0] - buf[11]);
    dfdt[0] = 0.0;
    dfdt[1] = 0.0;
    dfdt[2] = 0.0;
    dfdt[3] = 0.0;
    return GSL_SUCCESS;
}

double pars[14] = {5.0, 5.0, 0.02, 4.86e-6, 0.02, 0.28, 1500.0, 0.002, 0.0126,
  0.00335, 0.01, 3.0, 6317.1174, 7.79e-6};
double t[8], T[8], V[8], newT[8], newV[8], yInit[4];


double boostObj(const gsl_vector *x, void *params) {
  double k1 = gsl_vector_get(x, 0);
  double k8 = gsl_vector_get(x, 1);
  double p8 = gsl_vector_get(x, 2);
  double pV = gsl_vector_get(x, 3);
  double r = gsl_vector_get(x, 4);
  pars[2] = 2.505*M_2_PI*atan(r)+2.515;
  pars[3] = 0.01*M_2_PI*atan(k1)+0.01;
  pars[8] = 0.05*M_2_PI*atan(k8)+0.05;
  pars[9] = 0.005*M_2_PI*atan(p8)+0.005;
  pars[12] = 3150.0*M_2_PI*atan(pV)+5150;
  gsl_odeiv2_system odeSystem = {rhsF, jacF, 4, &pars};

  gsl_odeiv2_driver *d = gsl_odeiv2_driver_alloc_y_new(&odeSystem, gsl_odeiv2_step_msbdf,
      1e-8, 1e-8, 0.0);

  double t0 = 0.0;   
  double yi[4] = { yInit[0], yInit[1], yInit[2], yInit[3] };
  double sum = 0;
  int k = 1;

  for (int i = 1; i<=337; i++) {

    double ti = i;

    int status = gsl_odeiv2_driver_apply(d, &t0, ti, yi);
    if (status != GSL_SUCCESS){
        printf("error, return value=%d\n", status);
        break;
    }
    if (t[k] == ti){
      sum = sum + gsl_pow_2(T[k] - yi[0]) + 
        gsl_pow_2(V[k] - yi[3]);
      k++;
    }
    t0 = ti;
  }
  gsl_odeiv2_driver_free(d);

  return sum;
}

double obj(const gsl_vector *x, void *params){
  double k1 = gsl_vector_get(x, 0);
  double k8 = gsl_vector_get(x, 1);
  double p8 = gsl_vector_get(x, 2);
  double pV = gsl_vector_get(x, 3);
  double r = gsl_vector_get(x, 4);
  pars[2] = 2.505*M_2_PI*atan(r)+2.515;
  pars[3] = 0.01*M_2_PI*atan(k1)+0.01;
  pars[8] = 0.05*M_2_PI*atan(k8)+0.05;
  pars[9] = 0.005*M_2_PI*atan(p8)+0.005;
  pars[12] = 3150.0*M_2_PI*atan(pV)+5150;
  gsl_odeiv2_system odeSystem = {rhsF, jacF, 4, &pars};

  gsl_odeiv2_driver *d = gsl_odeiv2_driver_alloc_y_new(&odeSystem, gsl_odeiv2_step_msbdf,
      1e-8, 1e-8, 0.0);

  double t0 = 0.0;   
  double yi[4] = { yInit[0], yInit[1], yInit[2], yInit[3] };
  double sum = 0;
  int k = 1;

  for (int i = 1; i<=337; i++) {

    double ti = i;

    int status = gsl_odeiv2_driver_apply(d, &t0, ti, yi);
    if (status != GSL_SUCCESS){
        printf("error, return value=%d\n", status);
        break;
    }
    if (t[k] == ti){
      sum = sum + gsl_pow_2(gsl_log1p(newT[k]-1) - gsl_log1p(yi[0]-1)) + 
        gsl_pow_2(gsl_log1p(newV[k]-1) - gsl_log1p(yi[3]));
      k++;
    }
    t0 = ti;
  }
  gsl_odeiv2_driver_free(d);

  return sum;
}

int main(){
  gsl_set_error_handler_off();
  
  T[0] = 1138.0;
  T[1] = 822.118;
  T[2] = 674.043;
  T[3] = 515.74;
  T[4] = 541.27;
  T[5] = 520.82;
  T[6] = 571.9;
  T[7] = 612.76;

  V[0] = 0.0005;
  V[1] = 128.51978;
  V[2] = 24448.94096;
  V[3] = 1255.9432;
  V[4] = 173.36842;
  V[5] = 128.519789;
  V[6] = 16.5565;
  V[7] = 73.955419;


  t[0] = 0;
  t[1] = 10;
  t[2] = 17;
  t[3] = 24;
  t[4] = 31;
  t[5] = 38;
  t[6] = 175;
  t[7] = 337;

  gsl_vector *x0;
  gsl_vector *stepSize;
  gsl_vector *x;

  //setting initial guess
  x0 = gsl_vector_alloc(5);
  gsl_vector_set(x0, 0, -1329.89);
  gsl_vector_set(x0, 1, -0.993112);
  gsl_vector_set(x0, 2, -11.6698);
  gsl_vector_set(x0, 3, 2.68892);
  gsl_vector_set(x0, 4, -38.2526);
 
  //setting function to minimize
  gsl_multimin_function func;
  func.n = 5;
  func.f = &obj;
  func.params = NULL;
  //setting initial
  
  //setting step size

  stepSize = gsl_vector_alloc(5);
  gsl_vector_set(stepSize, 0, 5e-1);
  gsl_vector_set(stepSize, 1, 1e-02);
  gsl_vector_set(stepSize, 2, 1e-02);
  gsl_vector_set(stepSize, 3, 1e-02);
  gsl_vector_set(stepSize, 4, 5e-01);
 

  gsl_rng *rand = gsl_rng_alloc(gsl_rng_taus);
  gsl_rng_set(rand, 0.0);
  FILE *data = fopen("data/disp", "w");
  FILE *parsData = fopen("data/matPars", "w");
  FILE *meanData = fopen("data/meanData", "w");
  if (data == NULL) {
    printf("can't open file");
    return -1;
  }
  if (parsData == NULL) {
    printf("can't open file");
    return -1;
  }
  if (meanData == NULL) {
    printf("can't open file");
    return -1;
  }
  fprintf(data,"k1,k8,p8,pV,r\n");
  fprintf(parsData,"k1,k8,p8,pV,r\n");
  fprintf(meanData,"k1,k8,p8,pV,r\n");
  double koef1, koef2;
  double k1, k8, p8, pV, r, sigma;
  double matPars[5000][5];
  double mean[5];
  double deviat[5];
  size_t flag;
  yInit[0] = T[0];
  yInit[1] = 0;
  yInit[2] = 337.0;
  yInit[3] = 0.0005;
  for (int j = 0; j < 8; j++) {
    newT[j] = T[j];
    newV[j] = V[j];
  }
  sigma = obj(x0, NULL);
  for (int i = 1; i <= 5000; i++) {
    flag = 0;
    koef1 = gsl_ran_gaussian(rand, 1.0);
    koef2 = gsl_ran_gaussian(rand, 1.0);
    koef1 = exp(koef1*sqrt(sigma/16.0));
    koef2 = exp(koef2*sqrt(sigma/16.0));
    for (int j = 0; j < 8; j++) {
      newT[j] = koef1*T[j];
      newV[j] = koef2*V[j];
    }
    yInit[0] = newT[0];
    yInit[1] = 0;
    yInit[2] = 337.0;
    yInit[3] = newV[0];
        printf("%d: %e, %e\n", i, newT[0], newV[0]);
    gsl_multimin_fminimizer *sys = 
      gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2, 5);
    int k = gsl_multimin_fminimizer_set (sys, &func, x0, stepSize);
    int status, iter = 0;
    double size;
    do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate(sys);
      if (status) {
        printf("break\n");
        break;
      }
      size = gsl_multimin_fminimizer_size(sys);
      status = gsl_multimin_test_size(size, 1e-2);
      if (status == GSL_SUCCESS)
      {
        flag = 1;
        printf("suc\n");
      }
    }
    while (status == GSL_CONTINUE && iter < 1000);
    if (status != GSL_SUCCESS) {
      i--;
      continue;
    }
    if (flag) {
      //sigma = obj(sys->x, NULL);
      k1 = 0.01*M_2_PI*atan(gsl_vector_get (sys->x, 0)) + 0.01;
      k8 = 0.05*M_2_PI*atan(gsl_vector_get (sys->x, 1)) + 0.05;
      p8 = 0.005*M_2_PI*atan(gsl_vector_get (sys->x, 2)) + 0.005;
      pV = 3150*M_2_PI*atan(gsl_vector_get (sys->x, 3)) + 5150;
      r = 2.505*M_2_PI*atan(gsl_vector_get (sys->x, 4)) + 2.515;
      matPars[i-1][0] = k1;
      matPars[i-1][1] = k8;
      matPars[i-1][2] = p8;
      matPars[i-1][3] = pV;
      matPars[i-1][4] = r;
    }
    else {
        matPars[i-1][0] = k1;
        matPars[i-1][1] = k8;
        matPars[i-1][2] = p8;
        matPars[i-1][3] = pV;
        matPars[i-1][4] = r;
    }
    fprintf(parsData,"%e,%e,%e,%e,%e\n", k1, k8, p8, pV, r);
    printf("%10.3e  %10.3e  %10.3e  %10.3e  %10.3e  f() = %10.3e\n", k1,  k8, p8, pV,
        r, sys->fval);
    gsl_multimin_fminimizer_free(sys);
  }
  for (int i = 1; i < 5000; ++i) {
      
      for (int j = 0; j < 5; ++j) {
        mean[j] = 0.0;
        deviat[j] = 0.0;
      }

      //calc mean values
      for (int j = 0; j <= i; ++j) {
        for (int k = 0; k < 5; ++k)
          mean[k] += matPars[j][k];
      }
      for (int k = 0; k < 5; ++k)
        mean[k] /= i+1;

      //calc deviation
      for (int j = 0; j <= i; ++j) {
        for (int k = 0; k < 5; ++k)
          deviat[k] += gsl_pow_2(matPars[j][k] - mean[k]);
      }
      for (int k = 0; k < 5; ++k) {
        deviat[k] = sqrt(deviat[k]/i);
      }
      fprintf(meanData, "%e,%e,%e,%e,%e\n", mean[0], mean[1], mean[2], mean[3], mean[4]);
      fprintf(data, "%e,%e,%e,%e,%e\n", deviat[0], deviat[1], deviat[2], deviat[3],
          deviat[4]);
  }
  for (int k = 0; k < 5; ++k) {
    printf("[%e, %e]\n", mean[k] - 1.96*deviat[k], mean[k] + 1.96*deviat[k]);
  }
  fclose(data);
  fclose(parsData);
  gsl_rng_free(rand);
  return 0;
}
