#include"gsl_odeiv2.h"
#include"gsl_multifit_nlinear.h"
#include"gsl_matrix.h"
#include"gsl_math.h"
#include"gsl_vector.h"
#include"gsl_blas.h"
#include<stdio.h>

int N=8;

double val[32];

struct data{
    size_t n;
    double *t;
    double *y;
};

int rhsF(double t, const double y[], double f[], void *params){
    double *buf = (double*)params;
    double k = 1 - (y[0]+y[1])/buf[6];
    f[0] = buf[0] + buf[2]*y[0]*k - buf[3]*y[0]*y[3] - buf[4]*y[0];
    f[1] = buf[3]*y[0]*y[3] + buf[7]*y[1]*k - buf[8]*y[1]*y[2] - buf[5]*y[1];
    f[2] = buf[1] + buf[9]*y[1]*y[2] - buf[10]*y[2];
    f[3] = buf[12]*y[1] - buf[13]*y[0]*y[3] - buf[11]*y[3];
    return  GSL_SUCCESS;
}

int jacF(double t, const double y[], double *dfdy, double dfdt[], void *params){
    (void) (t);
    double *buf = (double*)params;
    gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, 4, 4);
    gsl_matrix *m = &dfdy_mat.matrix;
    gsl_matrix_set(m, 0, 0, buf[2]*(1 - (y[0]+y[1])/buf[6]) - buf[2]*y[0]/buf[6]
        - buf[3]*y[3] - buf[4]);
    gsl_matrix_set(m, 0, 1, -1.0*buf[2]*y[0]/buf[6]);
    gsl_matrix_set(m, 0, 2, 0.0);
    gsl_matrix_set(m, 0, 3, -1.0*buf[3]*y[0]);
    gsl_matrix_set(m, 1, 0, buf[3]*y[3] - buf[7]*y[1]/buf[6]);
    gsl_matrix_set(m, 1, 1, buf[7]*(1 - (y[0]+y[1])/buf[6]) - buf[7]*y[1]/buf[6]
        - buf[8]*y[2] - buf[5]);
    gsl_matrix_set(m, 1, 2, -1.0*buf[8]*y[1]);
    gsl_matrix_set(m, 1, 3, buf[3]*y[0]);
    gsl_matrix_set(m, 2, 0, 0.0);
    gsl_matrix_set(m, 2, 1, buf[9]*y[2]);
    gsl_matrix_set(m, 2, 2, buf[9]*y[1] - buf[10]);
    gsl_matrix_set(m, 2, 3, 0.0);
    gsl_matrix_set(m, 3, 0, buf[13]*y[3]);
    gsl_matrix_set(m, 3, 1, buf[12]);
    gsl_matrix_set(m, 3, 2, 0.0);
    gsl_matrix_set(m, 3, 3, -1.0*buf[13]*y[0] - buf[11]);
    dfdt[0] = 0.0;
    dfdt[1] = 0.0;
    dfdt[2] = 0.0;
    dfdt[3] = 0.0;
    return GSL_SUCCESS;
}

int main(){
  double params[14] = {5.0, 5.0, 0.02, 4.86e-6, 0.02, 0.28, 1500.0, 0.002,
    0.0126, 0.00335, 0.01, 3.0, 6317.1174, 7.79e-6};
  double k1 = 4.787e-06;
  double k8 = 2.511e-02;
  double p8 = 2.721e-04;
  double pV = 7586;
  double r = 5.168e-02;
  
  params[2] = r;
  params[3] = k1;
  params[8] = k8;
  params[9] = p8;
  params[12] = pV;
  gsl_odeiv2_system odeSystem = {rhsF, jacF, 4, &params};

  gsl_odeiv2_driver *d = gsl_odeiv2_driver_alloc_y_new(&odeSystem, gsl_odeiv2_step_msbdf,
      1e-8, 1e-8, 0.0);

  double t[8], y[16];

  y[0] = 1138.72;
  y[1] = 822.128;
  y[2] = 674.043;
  y[3] = 515.745;
  y[4] = 541.277;
  y[5] = 520.851;
  y[6] = 571.915;
  y[7] = 612.766;


  y[8] = 0.0;
  y[9] = 128.51978;
  y[10] = 24448.94096;
  y[11] = 1255.9432;
  y[12] = 173.36842;
  y[13] = 128.519789;
  y[14] = 16.5565;
  y[15] = 73.955419;

  
  t[0] = 0;
  t[1] = 10;
  t[2] = 17;
  t[3] = 24;
  t[4] = 31;
  t[5] = 38;
  t[6] = 175;
  t[7] = 337;

  int k = 1;
  double t0 = 0.0;   
  double yi[4] = { 1138.0, 0.0, 337.0, 0.0005 };
  double sum = 0;
  
  FILE *data = fopen("data/sol.dat", "w");
  if (data == NULL) {
    return 0;
  }
  fprintf(data, "t,T,T*,CTL,V\n");
  fprintf(data, "%f,%f,%f,%f,%f\n", t0, yi[0], yi[1], yi[2], yi[3]);
  for (int i = 1; i<=337; i++) {
      
      double ti = i;

      int status = gsl_odeiv2_driver_apply(d, &t0, ti, yi);
      if (status != GSL_SUCCESS){
          printf("error, return value=%d\n", status);
          break;
      }
      fprintf(data, "%e,%e,%e,%e,%e\n", ti, yi[0], yi[1], yi[2], yi[3]);
      if(t[k] == ti){
          printf("t = %.5e  Ti = %.5e Vi = %.5e   T = %.5e V = %.5e\n",t[k], y[k], y[k+8], yi[0], yi[3]);
          sum = sum + gsl_pow_2(gsl_log1p(y[k]-1) - gsl_log1p(yi[0]-1)) + 
            gsl_pow_2(gsl_log1p(y[k+8]-1) - gsl_log1p(yi[3]-1));
          
          k++;
      }
      t0 = ti;
  }
  printf("\n sum: %.5e\n", sum);
  gsl_odeiv2_driver_free(d);
  return 0;
}
