#include<math.h>
#include"gsl_odeiv2.h"
#include"gsl_multifit_nlinear.h"
#include"gsl_multimin.h"
#include"gsl_matrix.h"
#include"gsl_math.h"
#include"gsl_vector.h"
#include"gsl_blas.h"
#include<stdio.h>

int rhsF(double t, const double y[], double f[], void *params){
    double *buf = (double*)params;
    double k = 1 - (y[0]+y[1])/buf[6];
    f[0] = buf[0] + buf[2]*y[0]*k - buf[3]*y[0]*y[3] - buf[4]*y[0];
    f[1] = buf[3]*y[0]*y[3] + buf[7]*y[1]*k - buf[8]*y[1]*y[2] - buf[5]*y[1];
    f[2] = buf[1] + buf[9]*y[1]*y[2] - buf[10]*y[2];
    f[3] = buf[12]*y[1] - buf[13]*y[0]*y[3] - buf[11]*y[3];
    return  GSL_SUCCESS;
}

int jacF(double t, const double y[], double *dfdy, double dfdt[], void *params){
    (void) (t);
    double *buf = (double*)params;
    gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, 4, 4);
    gsl_matrix *m = &dfdy_mat.matrix;
    gsl_matrix_set(m, 0, 0, buf[2]*(1 - (y[0]+y[1])/buf[6]) - buf[2]*y[0]/buf[6]
        - buf[3]*y[3] - buf[4]);
    gsl_matrix_set(m, 0, 1, -1.0*buf[2]*y[0]/buf[6]);
    gsl_matrix_set(m, 0, 2, 0.0);
    gsl_matrix_set(m, 0, 3, -1.0*buf[3]*y[0]);
    gsl_matrix_set(m, 1, 0, buf[3]*y[3] - buf[7]*y[1]/buf[6]);
    gsl_matrix_set(m, 1, 1, buf[7]*(1 - (y[0]+y[1])/buf[6]) - buf[7]*y[1]/buf[6]
        - buf[8]*y[2] - buf[5]);
    gsl_matrix_set(m, 1, 2, -1.0*buf[8]*y[1]);
    gsl_matrix_set(m, 1, 3, buf[3]*y[0]);
    gsl_matrix_set(m, 2, 0, 0.0);
    gsl_matrix_set(m, 2, 1, buf[9]*y[2]);
    gsl_matrix_set(m, 2, 2, buf[9]*y[1] - buf[10]);
    gsl_matrix_set(m, 2, 3, 0.0);
    gsl_matrix_set(m, 3, 0, buf[13]*y[3]);
    gsl_matrix_set(m, 3, 1, buf[12]);
    gsl_matrix_set(m, 3, 2, 0.0);
    gsl_matrix_set(m, 3, 3, -1.0*buf[13]*y[0] - buf[11]);
    dfdt[0] = 0.0;
    dfdt[1] = 0.0;
    dfdt[2] = 0.0;
    dfdt[3] = 0.0;
    return GSL_SUCCESS;
}

double pars[14] = {5.0, 5.0, 0.02, 4.86e-6, 0.02, 0.28, 1500.0, 0.002, 0.0126,
  0.00335, 0.01, 3.0, 6317.1174, 7.79e-6};
double t[8], y[16];


double obj(const gsl_vector *x, void *params){
  double k1 = gsl_vector_get(x, 0);
  double k8 = gsl_vector_get(x, 1);
  double p8 = gsl_vector_get(x, 2);
  double pV = gsl_vector_get(x, 3);
  double r = gsl_vector_get(x, 4);
  pars[2] = 2.505*M_2_PI*atan(r)+2.515;
  pars[3] = 0.01*M_2_PI*atan(k1)+0.01;
  pars[8] = 0.05*M_2_PI*atan(k8)+0.05;
  pars[9] = 0.005*M_2_PI*atan(p8)+0.005;
  pars[12] = 3150.0*M_2_PI*atan(pV)+5150;

  gsl_odeiv2_system odeSystem = {rhsF, jacF, 4, &pars};

  gsl_odeiv2_driver *d = gsl_odeiv2_driver_alloc_y_new(&odeSystem, gsl_odeiv2_step_msbdf,
      1e-8, 1e-8, 0.0);

  double t0 = 0.0;   
  double yi[4] = { 1138.0, 0.0, 337.0, 0.0005 };
  double sum = 0;
  int k = 1;

  for (int i = 1; i<=337; i++) {

    double ti = i;

    int status = gsl_odeiv2_driver_apply(d, &t0, ti, yi);
    if (status != GSL_SUCCESS){
        printf("error, return value=%d\n", status);
        break;
    }
    if(t[k] == ti){
      //sum = sum + gsl_pow_2(gsl_log1p(y[k]-1) - gsl_log1p(yi[0]-1)) + gsl_pow_2(gsl_log1p(y[k+8]-1) - gsl_log1p(yi[3]));
      sum = sum + gsl_pow_2(y[k] - yi[0]) + gsl_pow_2(y[k+8] - yi[3]);
      k++;
    }
    t0 = ti;
  }
  gsl_odeiv2_driver_free(d);

  return sum;
}

int main(){
  y[0] = 1138.72;
  y[1] = 822.128;
  y[2] = 674.043;
  y[3] = 515.745;
  y[4] = 541.277;
  y[5] = 520.851;
  y[6] = 571.915;
  y[7] = 612.766;


  y[8] = 0;
  y[9] = 128.51978;
  y[10] = 24448.94096;
  y[11] = 1255.9432;
  y[12] = 173.36842;
  y[13] = 128.519789;
  y[14] = 16.5565;
  y[15] = 73.955419;

  
  t[0] = 0;
  t[1] = 10;
  t[2] = 17;
  t[3] = 24;
  t[4] = 31;
  t[5] = 38;
  t[6] = 175;
  t[7] = 337;


  gsl_multimin_fminimizer *sys = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2, 5);
  if (sys == NULL) {
    printf("not enough memory\n");
    return -1;
  }

  gsl_vector *x0;
  gsl_vector *stepSize;
  gsl_vector *x;

  //setting initial guess


  x0 = gsl_vector_alloc(5);
/*
  gsl_vector_set(x0, 0, 7.3e-06);
  gsl_vector_set(x0, 1, 0.0187);
  gsl_vector_set(x0, 2, 0.0003);
  gsl_vector_set(x0, 3, 5300);
  gsl_vector_set(x0, 4, 0.2);
*/

  gsl_vector_set(x0, 0, -872.081);
  gsl_vector_set(x0, 1, -1.50171);
  gsl_vector_set(x0, 2, -10.5789);
  gsl_vector_set(x0, 3, 0.07493964001);
  gsl_vector_set(x0, 4, -159.471);
  


  //setting function to minimize
  gsl_multimin_function func;
  func.n = 5;
  func.f = &obj;
  func.params = NULL;

  //setting step size
  stepSize = gsl_vector_alloc(5);
  gsl_vector_set(stepSize, 0, 1);
  gsl_vector_set(stepSize, 1, 1e-02);
  gsl_vector_set(stepSize, 2, 1e-02);
  gsl_vector_set(stepSize, 3, 1e-02);
  gsl_vector_set(stepSize, 4, 1);
  
  //setting minimizer
  int k = gsl_multimin_fminimizer_set (sys, &func, x0, stepSize);
  int status, iter = 0;
  double size;
  do
  {
    iter++;
    status = gsl_multimin_fminimizer_iterate(sys);
    if (status) {
      break;
    }

    size = gsl_multimin_fminimizer_size(sys);
    status = gsl_multimin_test_size (size, 1e-2);
    
    if (status == GSL_SUCCESS)
    {
      printf("min at\n");
    }
    printf("%5d %10.3e %10.3e %10.3e %10.3e %10.3e f() = %7.3f size = %.3f\n",
        iter,
        0.01*M_2_PI*atan(gsl_vector_get (sys->x, 0)) + 0.01,
        0.05*M_2_PI*atan(gsl_vector_get (sys->x, 1)) + 0.05,
        0.005*M_2_PI*atan(gsl_vector_get (sys->x, 2)) + 0.005,
        3150*M_2_PI*atan(gsl_vector_get (sys->x, 3)) + 5150,
        2.505*M_2_PI*atan(gsl_vector_get (sys->x, 4)) + 2.515,
        sys->fval, size);
  }
  while (status == GSL_CONTINUE && iter < 10000);
  
  gsl_multimin_fminimizer_free(sys);
  return 0;
}
